const config = {
  defaultTitle: 'John Doe',
  url:
    process.env.NODE_ENV !== 'development'
      ? process.env.NEXT_PUBLIC_PORTFOLIO_URL
      : 'http://localhost:3040',
  defaultDescription: 'Ironic Success',
  googleAnalyticsID: 'G-PE8QW0ZGTZ',
};

export default config;
