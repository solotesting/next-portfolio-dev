import Link from 'next/link';
import { useCustomTheme } from 'providers/ThemeProvider';
import Container from 'components/ui/Container';
import Button from 'components/ui/Button';
import { Wrapper, SkillsWrapper, Details, Thumbnail } from './styles';

const Skills = () => {
  const theme = useCustomTheme();

  return (
    <Wrapper id="about">
      <SkillsWrapper as={Container}>
        <Thumbnail>
          <img
            src="assets/illustrations/mindset.svg"
            alt="Discover skills everyone needs before entering into a new career path"
          />
        </Thumbnail>
        <Details theme={theme}>
          <h1>Soft SKills, Meta Skills, and More</h1>
          <p>
            The biggest advice I wish someone told me is, "DO you consider yourself a good problem solver?"
          </p>
          <Link href="#contact">
            <Button as="a">Subscribe</Button>
          </Link>
        </Details>
      </SkillsWrapper>
    </Wrapper>
  );
};

export default Skills;
