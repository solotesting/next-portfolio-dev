import Link from 'next/link';
import Button from 'components/ui/Button';
import Container from 'components/ui/Container';
import Header from 'components/ui/theme/Header';
import { useCustomTheme } from 'providers/ThemeProvider';
import { Wrapper, IntroWrapper, Details, Thumbnail } from './styles';

const Intro = () => {
  const theme = useCustomTheme();

  return (
    <Wrapper>
      <Header />
      <IntroWrapper as={Container}>
        <Details theme={theme}>
          <h1>Ironic Success</h1>
          <h2>Musings from the real scenarios</h2>
          <Link href="#contact">
            <Button as="a">Subscribe</Button>
          </Link>
        </Details>
        <Thumbnail>
          <img src="assets/illustrations/world.svg" alt="Nick" />
        </Thumbnail>
      </IntroWrapper>
    </Wrapper>
  );
};

export default Intro;
