import Container from 'components/ui/Container';
import social from './social.json';
import { Wrapper, Flex, Links, Details } from './styles';

const Footer = () => (
  <Wrapper>
    <Flex as={Container}>
      <Details>
        <h2>Ironic Success</h2>
      </Details>
      <Links>
        {social.map(({ id, name, link, icon }) => (
          <a
            key={id}
            href={link}
            target="_blank"
            rel="noopener noreferrer"
            aria-label={`follow me on ${name}`}
          >
            <img width="24" src={icon} alt={name} />
          </a>
        ))}
      </Links>
    </Flex>
  </Wrapper>
);

export default Footer;
